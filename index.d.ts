/**
 * Created by DICH on 7/22/2017.
 */
export declare function session(options) : void
export declare namespace SessionStoreManager {
    function getSession(sessionId, callback) : void
    function saveSession(sessionId, session, callback) : void
    function clearSession(sessionId, callback) : void
    function updateUserSessionInfo(userId, updateInfo, callback) : void
    function pdateUserSessionCountField(userId, updateInfo, callback) : void
    function saveAuthenInfo(user, sessionId, browserId, ip) : void
    var mappingUseridSessionid : any
}