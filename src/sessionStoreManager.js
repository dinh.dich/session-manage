/**
 * Created by DICH on 3/10/2017.
 */
var sessionStore;
var mappingUseridSessionid = {};

var SessionStoreManager = function (store) {
    sessionStore = SessionStoreManager.sessionStore = store;
    return SessionStoreManager;
}

SessionStoreManager.getSession = function (sessionId, callback) {
    sessionStore.get(sessionId, function (error, session) {
        callback(error, session)
    })
}

SessionStoreManager.saveSession = function (sessionId, session, callback) {
    sessionStore.set(sessionId, session, callback)
}

SessionStoreManager.clearSession = function (sessionId, callback) {
    sessionStore.get(sessionId, function (error, session) {
        if (session) {
            sessionStore.destroy(sessionId);
            delete mappingUseridSessionid[sessionId];
            if(session.user) {
                delete mappingUseridSessionid[session.user.user_id][sessionId];
            }
            callback && callback();
        } else {
            callback && callback();
        }
    });
};

SessionStoreManager.updateUserSessionInfo = function (userId, updateInfo, callback) {
    if (mappingUseridSessionid[userId]) {
        for(var sessionId in mappingUseridSessionid[userId]) {
            sessionStore.get(sessionId, function (error, session) {
                if (session && session.user) {
                    for (var field in updateInfo) {
                        session.user[field] = updateInfo[field];
                    }

                    sessionStore.set(sessionId, session, function (error) {
                        callback && callback(error);
                    });
                } else {
                    callback && callback();
                }
            });
        }
    } else {
        callback && callback();
    }
};

SessionStoreManager.updateUserSessionCountField = function (userId, updateInfo, callback) {
    if (mappingUseridSessionid[userId]) {
        for(var sessionId in mappingUseridSessionid[userId]) {
            sessionStore.get(sessionId, function (error, session) {
                if (session && session.user) {
                    for (var field in updateInfo) {
                        session.user[field] += updateInfo[field];
                    }

                    sessionStore.set(sessionId, session, function (error) {
                        callback && callback(error);
                    });
                } else {
                    callback && callback();
                }
            });
        }
    } else {
        callback && callback();
    }
};

SessionStoreManager.saveAuthenInfo = function (user, sessionId, browserId, ip) {
    mappingUseridSessionid[sessionId] = user.user_id;
    if (!mappingUseridSessionid[user.user_id]) {
        mappingUseridSessionid[user.user_id] = {};
    }
    mappingUseridSessionid[user.user_id][sessionId] = {
        browser_id: browserId,
        ip: ip
    };
}

/**
 * call when session expire
 * @param sessionId
 * @param user
 */
SessionStoreManager.releaseSession = function (sessionId, user) {
    if(mappingUseridSessionid[user.user_id]) {
        delete mappingUseridSessionid[user.user_id][sessionId];
    }
    delete mappingUseridSessionid[sessionId];
}

/**
 * call when user logout or be forced logout
 * @param userId
 * @param callback
 */
SessionStoreManager.logoutUser = function (userId, callback) {
    if (mappingUseridSessionid[userId]) {
        for(var sessionId in mappingUseridSessionid[userId]) {
            SessionStoreManager.clearSession(sessionId, function (error) {
                //TODO
            })
        }

        delete mappingUseridSessionid[userId];
        callback && callback();
    } else {
        callback && callback();
    }
}

SessionStoreManager.mappingUseridSessionid = mappingUseridSessionid;

module.exports = SessionStoreManager;
