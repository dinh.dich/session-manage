/**
 * Created by Administrator on 10/11/2015.
 */
var _this = this;

/**
 *
 * @param req
 * @return {params}
 */
exports.getCorrectParam = function (req) {
    var param;
    if(req.body.param) {
        try {
            param = JSON.parse(req.body.param);
            //param = (req.body.param);
            if (!param) {
                param = {data: {}};
            }
            //console.log(param);
        } catch (e) {
            console.log(e);
            param = {data: {}};
        }
    } else {
        param = {data: {}};
    }
    return param;
};

exports.getSourceIp = function (req) {
    var ip = req.headers['X-Real-IP'] || req.connection.remoteAddress;

    if(ip == '127.0.0.1') {
        return _this.getSourceIpBehindNginxProxy(req)
    }

    if (!ip) {
        res.end();
        return;
    }
    ip = ip.replace('::ffff:', '');
    if (ip == '::1') {
        ip = _this.getSourceIpBehindNginxProxy(req) || '127.0.0.1';
    }

    return ip;
};

exports.getSourceIpBehindNginxProxy = function (req) {
    var ip = req.headers['x-forwarded-for'] || req.headers['X-Real-IP'] || req.connection.remoteAddress;
    ip = ip.replace('::ffff:', '');
    if (ip == '::1') {
        ip = '127.0.0.1';
    }

    return ip;
};

