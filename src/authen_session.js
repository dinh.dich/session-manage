/**
 * Created by DICH on 2/15/2017.
 */
var requestHelper = require('./request_helper');
var cryptoUtil = require('./crypto_util')
var uid = require('uid-safe').sync;
var SessionStatus = require('../constants/SessionStatus');
var sessionManager = {};
var authenService = {};
var systemName = '';

var GUEST_ACCOUNT = {
    user_id: 'guest',
    is_guest: true
};

module.exports = function (service, sessionStoreManager, sysName) {
    authenService = service;
    sessionManager = sessionStoreManager;
    systemName = sysName;
    return function (req, res, next) {
        //get ip
        var ip = requestHelper.getSourceIp(req);

        isLogged(req, res, ip, function (status, user, tokenInfo) {
            if (status && user) {
                next(null, tokenInfo && tokenInfo.sid);
            } else {
                loginAsGuest(uid(10), ip, req, res, next)
            }
        })
    }
}

function loginAsGuest(browserId, ip, req, res, next) {
    var user = Object.assign({}, GUEST_ACCOUNT);
    user.user_id = 'guest';
    user.name = 'Guest';
    user.token_ser = uid(25);
    user.ip = ip;
    user.sid = req.sessionID;
    user.browser_id = browserId;
    req.session.user = user;

    req.session.SESSION_STATUS = SessionStatus.NEW_GUEST;
    sessionManager.saveAuthenInfo(user, req.sessionID, browserId, ip);

    next();
}

/**
 *
 * @param req
 * @param res
 * @param ip
 * @param {status, user, tokenInfo} callback
 */
function isLogged(req, res, ip, callback) {
    if (typeof ip == 'function') {
        callback = ip;
        ip = null;
    }

    if (req.session.user && req.session.user.user_id != 'guest') {
        req.session.SESSION_STATUS = SessionStatus.OLD_SESSION;
        callback(true, req.session.user);
    } else {
        var loginToken = req.cookies['connect.gid'] || ''
        var bid = req.cookies['BID'] || ''
        if (loginToken.length > 0) {
            authenService.callMethod('checkLogin', [loginToken, bid], function (error, tokenInfo, userInfo) {
                if(tokenInfo && userInfo) {
                    userInfo.token_ser = loginToken;
                    userInfo.ip = res.locals.ip;
                    userInfo.sid = req.sessionID;
                    userInfo.gid = loginToken;
                    userInfo.browser_id = bid;

                    req.session.user = userInfo;
                    req.session.SESSION_STATUS = SessionStatus.JUST_LOGIN;

                    sessionManager.saveSession(tokenInfo.sid, req.session, function () {
                        //TODO
                    })
                    sessionManager.saveAuthenInfo(userInfo, tokenInfo.sid, bid, ip);
                    sessionManager.clearSession(req.sessionID)

                    callback(true, userInfo, tokenInfo)
                } else {
                    callback(false)
                }
            })
        } else {
            callback(false);
        }
    }
};
