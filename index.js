/**
 * Created by DICH on 3/13/2017.
 */
var _defaults = require('lodash').defaults;

exports.session = function (options) {
    options = _defaults({}, options, {
        secret: "my-secret",
        resave: false,
        saveUninitialized: false,
        key: 'SID',
        cookie: {
            maxAge: 60 * 60000,     //60 minutes
            domain: options.domain
        }
    });

    try {
        var app = require(options.app);
        options.key = app.get('sessionIDName') || options.key;
    } catch (e) {
        console.log(e);
        throw new Error('session-manager require app param refer to server.js file')
    }

    var expressSession = require('express-session-expire-timeout')(options);
    var sessionStore = expressSession.store;
    var authenService = {
        callMethod: function (methodName, params, callback) {
            app.serviceCaller && app.serviceCaller.call('authen', 'authen.' + methodName, params, callback);
        }
    }
    var sessionStoreManager = require('./src/sessionStoreManager')(sessionStore);

    sessionStore.setOnDestroySession(function (sessionId, session) {
        if (session && session.user) {
            sessionStoreManager.releaseSession(sessionId, session.user);
        }
    });

    var _session = expressSession.session;
    var _authenSession = require('./src/authen_session')(authenService, sessionStoreManager, app.get('systemName'));

    return function (req, res, next) {
        _session(req, res, function (error) {
            if(error) return next(error)
            _authenSession(req, res, function (error, sid) {
                if(sid) {
                    req.sessionID = sid
                }
                next()
            })
        })
    }
};

exports.SessionStoreManager = require('./src/sessionStoreManager');
